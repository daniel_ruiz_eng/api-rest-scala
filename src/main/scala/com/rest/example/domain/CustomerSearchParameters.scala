package com.rest.example.domain

import java.util.Date

/**
  * Customers search parameters.
  *
  * Establece una case class para establecer parametros para realizar la busqueda
  *
  * @param firstName first name
  * @param lastName  last name
  * @param birthday  date of birth
  */
case class CustomerSearchParameters(
                                   firstName: Option[String] = None,
                                   lastName: Option[String] = None,
                                   birthday: Option[Date] = None
                                   )
