package com.rest.example.domain


import scala.slick.driver.MySQLDriver.simple._
/**
  * Customer entity.
  *
  * @param id        unique id
  * @param firstName first name
  * @param lastName  last name
  * @param birthday  date of birth
  */

// Definimos case class con parametros id y birthdate opcionales
case class Customer(id: Option[Long], firstName: String, lastName: String, birthday:Option[java.util.Date])


/**
  * Mapped customer table object
  * Like jpa usando libreria slick, definimos objeto, que extiende de Table de tipo case class que definimos
  */

object Customers extends Table[Customer]("customers") {

  // O es clase tipo Option columns que definie opciones de columnas para mysql

  // Defino mapper para fechas
  implicit val dateTypeMapper = MappedTypeMapper.base[java.util.Date, java.sql.Date] (
    {
      ud => new java.sql.Date(ud.getTime) // Funcion Lambda que especifica como pasar un util.date a un sql.Date
    },
    {
      sd => new java.util.Date(sd.getTime) // Funcion Lambda que especifica como pasar un util.date a un sql.Date
    }
  )


  // Defino columnas de la tabla

  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

  def firstName = column[String]("first_name")

  def lastName = column[String]("last_name")


  def birthday = column[java.util.Date]("birthday", O.Nullable)

  // Defino get all from database Mapeando los atributos con case class
  // Unnaply coge el objeto y retorna argumentos
  // Al final estoy definiendo un mapeo bidireccional apply mete parametros en objeto y unnaply mete atributos del objeto en parametros
  def * = id.? ~ firstName ~ lastName ~ birthday.? <> (Customer, Customer.unapply _)

  // Defino operadores
  // Template de slick que coge parametros y retorna query
  val findById = for {
    id <- Parameters[Long]
    c <- this if c.id is id
  } yield c

}