package com.rest.example.config

import com.typesafe.config.ConfigFactory

import scala.util.Try

trait Configuration {

  /**
    *  APlication config object
    */

  val config = ConfigFactory.load() // De manera predeterminada busca un el archivo application.conf,
                                    // si se llama de otra forma hay que pasar el nombre como parametro en el load

  /**
    * Host name/address to start service on
    */
  lazy val serviceHost = Try(config.getString("service.host")).getOrElse("localhost")

  /**
    * Puerto
    */
  lazy val servicePort = Try(config.getString("service.port")).getOrElse(8080)

  /**
    * Host DB
    */
  lazy val dbHost = Try(config.getString("db.host")).getOrElse("localhost")

  /**
    * port number DB
    */
  lazy val dbPort = Try(config.getString("db.port")).getOrElse(3306)

  /**
    * DB name
    */
  lazy val dbName = Try(config.getString("db.name")).getOrElse("rest")

  /**
    * DB user
    */
  lazy val dbUser = Try(config.getString("db.name")).toOption.orNull


  /**
    * DB password
    */
  lazy val dbPassword = Try(config.getString("db.password")).toOption.orNull


}
